#  Calculatrice à notation polonaise inverse

Mettez-vous en binôme, vous allez faire du TDD en mode ping-pong.

La notation polonaise inverse (Reverse Polish Notation en anglais) également connue sous le nom de notation post-fixée permet d'écrire de façon non ambiguë les formules arithmétiques sans utiliser de parenthèses.

Vous allez implémenter la classe suivante : 

``` java
public class RPNCalculator {
    public static double eval (String expression) {
        ...
    }
}
```

## Initialisation

### configuration de maven

* s'il n'existe pas déjà créez un dossier ".m2" : `mkdir $HOME/.m2`
* création du fichier de configuration : `touch $HOME/.m2/settings.xml`
* avec votre éditeur de texte préféré, mettez ce qu'il y a ci-dessous dans `settings.xml`
* finalement, vérifiez votre installation : `mvn -version`

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <proxies>
      <proxy>
        <id>proxy</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>cache.univ-lille1.fr</host>
        <port>3128</port>
      </proxy>
    </proxies>
</settings>
```

### Projet

* Si ce n'est pas déjà fait, demandez à rejoindre le groupe gitlab suivant : https://gitlab.univ-lille.fr/2019-coa-tdd/groupe-[Votre groupe]/
* Créez un projet dans ce groupe, nommé `tp1-nom1-nom2`
* Dans un terminal, si ce n'est pas déjà fait, créez un workspace pour la coa-tdd `mkdir ~/ws-coa-tdd`
* Clonez le squelette de projet maven `git clone https://gitlab.univ-lille.fr/2019-coa-tdd/maven-skeleton.git ~/ws-coa-tdd/tp1-nom1-nom2`
* Dans un terminal, entrez dans le répertoire `~/ws-coa-tdd/tp1-nom1-nom2`
* Changer l'URL du projet gitlab pour y mettre la votre `git remote set-url origin URL`
* Modifier le fichier `pom.xml` changez le nom du projet dans le fichier pom.xml `sed -i -e 's/maven-skeleton/tp1-nom1-nom2/g' pom.xml`
* Lancez les tests pour vérifier que tout fonctionne `mvn test`
* Ouvrez éclipse `eclipse -data ~/ws-coa-tdd/`
* Importez votre nouveau projet maven à l'aide du menu File > Import.

Une fois arrivé là, prévenir l'enseignant et allez aider les autres binômes. L'objectif est de commencer l'étape d'après tous ensemble.

### Barème de relecture

Dans toute la suite attention de bien suivre la boucle TDD et de faire des commits à la fin de chaque étape, et des push à la fin de chaque boucle. Un git log du projet devrait ressembler à ça :


```
...
efd6601 Add test and impl for minus
b28a944 Refactor tests
b28a944 True implementation for plus
8ab9f43 Add second test for plus
b28a944 Refactor static import in tests
b28a944 Very simple implementation for plus
2530399 Add first test for plus
f9aa3f3 Bootstrap project

```

Nous apporterons une attention toute particulière à la concordance entre les messages de commits, les heures de commits et les contenus des commits.

Pendant la suite du TP, nous allons ajouter un certain nombre de règles très simples permettant d'avoir un code plus propre, le respect des ces règles rentre bien évidemment dans le barème de correction.

Enfin, chaque push va alimenter sonar : https://sonar.tcweb.org/. Sonar est un robot capable de détecter des bugs, des vulnérabilités ainsi que la couverture de code et le pourcentage de code dupliqué. Dans le cadre du TP il est important d'obtenir "A" dans toutes les notes, 100% de couverture de code et 0% de code dupliqué.

## Addition

Avec l'expression suivante en entrée, la méthode `RPNCalculator.eval()` va nous retourner le résultat de l'opération, par exemple :

| Entrée      | Sortie | Notation infixée |
|-------------|--------|------------------|
| 1 1 +       | 2      | 1 + 1            |
| 7 3 +       | 10     | 7 + 3            |
| 9 13 +      | 22     | 9 + 13           |
| 13.9 1.3 +  | 15.2   | 13.9 + 1.3       |

## Soustraction

Avec l'expression suivante en entrée, la méthode `RPNCalculator.eval()` va nous retourner le résultat de l'opération, par exemple :

| Entrée      | Sortie | Notation infixée |
|-------------|--------|------------------|
| 4 1 -       | 3      | 4 - 1            |
| 7 9 -       | -2     | 7 - 9            |
| 139.7 12 -  | 127.7  | 139.7 - 12       |

## De multiples opérations

Avec l'expression suivante en entrée, la méthode `RPNCalculator.eval()` va nous retourner le résultat de l'opération, par exemple :

| Entrée      | Sortie | Notation infixée |
|-------------|--------|------------------|
| 1 2 + 6 +   | 9      | ( 1 + 2 ) + 6    |
| 7 3 5 - +   | 5     | 7 + (3 - 5)      |

## Multiplication

Avec l'expression suivante en entrée, la méthode `RPNCalculator.eval()` va nous retourner le résultat de l'opération, par exemple :

| Entrée      | Sortie | Notation infixée |
|-------------|--------|------------------|
| 2 2 *       | 4      | 2 * 2            |
| 9 0.5 *     | 4.5    | 9 * 0.5          |
| 1359 1.1 *  | 1494.9 | 1359 * 1.1       |
| 7 3 * 4 +   | 25     | (7 * 3) + 4      |
| 7 3 4 * +   | 19     | 7 + (3 * 4)      |

## Division

Avec l'expression suivante en entrée, la méthode `RPNCalculator.eval()` va nous retourner le résultat de l'opération, par exemple :

| Entrée      | Sortie    | Notation infixée |
|-------------|-----------|------------------|
| 2 4 /       | 0.5       | 2 / 4            |
| 19.4 3 /    | 6.4666... | 19.4 / 3         |
| 18 3 / 2 +  | 8         | 18 / 3 + 2       |
| 18 3 2 / -  | 16.5      | 18 - (3 / 2)     |

## Racine carrée

Ajouter l'opérateur SQRT qui calcul la racine carrée de l'opérande.

## Trigonométrie

Ajouter les opérateurs SIN, COS et TAN qui calcul respectivement le sinus, le cosinus et la tangente de l'opérande.

## Puissance

Ajouter l'opérateur ^ qui calcule la puissance des 2 opérandes


| Entrée      | Sortie | Notation infixée  |
|-------------|--------|-------------------|
| 2 3 ^       | 8      | 2 ^ 3 = 2 * 2 * 2 |

## Puissances itérées de Knuth

Ajouter l'opérateur ↑↑ qui calcule la puissance itérée sur les 2 opérandes

Pour obtenir le caractère ↑ faire CTRL + MAJ + U puis 2191 puis ENTRÉE


| Entrée      | Sortie | Notation infixée       |
|-------------|--------|------------------------|
| 2 4 ↑↑      | 65536  | 2 ↑↑ 4 = 2 ^ 2 ^ 2 ^ 2 |


# Barème de correction

Comme une dictée, la copie démarre avec 20 points. Chaque erreur enlève des points.

Dans Sonar :
* -3 par bug ou vulnérabilité bloquante
* -2 par bug ou vulnérabilité critique
* -1 par bug ou vulnérabilité majeur
* -0.5 par bug ou vulnérabilité pour tous les autres types.
* -0.5 par pourcentage de code non couvert
* -0.5 par pourcentage de code dupliqué

L'ensemble du code doit respecter des règles que nous avons eues (SOLID, lisibilité, DRY, KISS, YAGNI, etc.). En fonction de ces règles, estimez la note du code que vous relisez.

Un test rouge : -1 point

Je vous invite à ajouter vos propres tests pour vérifier que le code respecte bien le besoin métier. Attention le but est bien d'évaluer la qualité du code et pas sa complétude en rapport à toutes les questions. Si vous avez des tests correspondant aux questions traités par les étudiants que vous corrigez qui ne passent pas vous pouvez enlever des points.

Pour vérifier que la boucle TDD a bien été respectée, vous avez 2 outils à votre disposition :
* `git log --oneline --decorate --graph --all --format="%h %C(auto,blue)%ad%C(auto,black) %s"` qui permet de lister tous les commits avec l'heure et le commentaire
* `for commit in $(git log --reverse --pretty=format:%H); do git checkout $commit; git show --stat $commit; read; done` qui rejoue la liste de tous les commit 1 à 1 en affichant quelques éléments. Pour passer d'un commit à l'autre, il faut sortir du git show et valider avec la touche entrée.

À chaque non respect de la boucle TDD -1 point s'impose.

Merci de remplir un fichier notes.md à la racine du projet que vous corrigez avec le contenu suivant :


```markdown
| Correcteur        | Note /20 | Commentaire |
|-------------------|----------|-------------|
| Nom               | XX       |             |
| Nom               | XX       |             |
```

Je vais moi aussi noter votre code, si je trouve des notes trop gentilles ou trop sévère c'est 0 aux 2 copies.

## Une solution <!-- omit in toc -->

https://tube.tux.ovh/videos/watch/c932efda-7880-442a-8482-b4d56ebad7d6

## Rappel des règles de clean code vue pendant le TP

* pas de méthode de plus de 10 lignes
* nommage correct des variables et des méthodes : on comprend à quoi elles servent à la lecture et il n'y a pas d'abréviation.
* pas plus de 3 indentations par méthode
* pas de classe de plus de 50 lignes
* pas de commentaire, on comprend le code à la première lecture
* pas de répétition (DRY)
* le code est le plus simple possible : si je peux enlever du code sans casser les tests c'est que je n'ai pas fini ma simplification
* Pas de "magic numbers", des nombre ou des Strings qui sortent de nulle part.
* Max 2 paramètres par méthode
* Pas plus d'un point par ligne (cf. notation fluent)



